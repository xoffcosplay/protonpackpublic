// Proton Pack Lights Redux
//
// Rewrite of Proton Pack lighting, without the use of delay
//
// Chris Kurtz (chk@xoff.org)
// November 2016
//
// Timing values from HyperDyne Lab's Integrated Controller PDF:
// http://www.hyperdynelabs.com/docs/GB%20integrated%20controller.pdf
//
// Libraries:
#include <FastLED.h>

// Cyclotron:
	// Default value is 250ms per LED
	// Suggested delays: 700ms/LED, 600ms/LED, 500ms/LED, 400 ms/LED, 300ms/LED, 250ms/LED (default), 200ms/LED, 175ms/LED, 150ms/LED, 100ms/LED
	// Timing for the LEDs, above - multiply by 4 for some reason?
	const int cyclotronTiming = 250;
	// const int cyclotronTiming = 35;
	// NUM_LEDS_Cyclotron will change if you're using single LEDs or jewels
	// A jewel is 7 LEDs, so 7*4=28
	const int cyclotron_NUM_LEDS = 28;
	// Pin for the Cyclotron LEDs
	const int cyclotron_PIN = 12;
 	// Timer for Cyclotron
	unsigned long cyclotronPreviousMillis = 0;
	// Which Element of array we're on
	int cyclotronElement = 0;
 	// Initialize the cyclotron LED array
	CRGB cyclotronLEDS[cyclotron_NUM_LEDS];

// Power cell:
	// Default value is 28ms per LED
	// Suggested delays: 50ms/LED, 45ms/LED, 42ms/LED, 40ms/LED, 38ms/LED, 35ms/LED, 30ms/LED (default), 28ms/LED, 25ms/LED, 20ms/LED
	// Timing for the LEDs, above - multiply by 4 for some reason?
	const int powercellTiming = 120;
	// Based on the length/density of LED strip you're using
	const int powercell_NUM_LEDS = 16;
	// Pin for the Powercell LEDs
	const int powercell_PIN = 11;
 	// Timer for Powercell
	unsigned long powercellPreviousMillis = 0;
	// Which Element of array we're on
	int powercellElement = 0;
 	// Initialize the powercell LED array
	CRGB powercellLEDS[powercell_NUM_LEDS];

void setup() {
  // Initialize the LED strips and clear them
  FastLED.addLeds<NEOPIXEL,cyclotron_PIN>(cyclotronLEDS,cyclotron_NUM_LEDS);
  FastLED.addLeds<NEOPIXEL,powercell_PIN>(powercellLEDS,powercell_NUM_LEDS); 
  FastLED.clear();
  // TEMP  - just because it's darned bright in testing!
  FastLED.setBrightness(30);
  // TEMP - debugging
  Serial.begin(9600);
}

void loop() {
	// current time (ms since boot)
	unsigned long currentMillis = millis();

	// Cyclotron lights
	// If it's been cyclotronTiming ms since the last cyclotron action, do another action
	if(currentMillis - cyclotronPreviousMillis >= cyclotronTiming)
	{
		// Turn on the next LED
		Serial.print("Turning on cyclotronElement ");
		Serial.println(cyclotronElement);
		cyclotronLEDS[cyclotronElement] = CRGB::Red;
		FastLED.show();
		// If we have a full jewel turn it off
		// Easy way to see if we've got a full jewel is to divide by the number of elements
		// in the jewel, remembering we need to add one so we're dividing by 1-28 and not 
		// 0-27 (the actual array.) If we have no remainder then the star is full
		if(((cyclotronElement + 1) % 7) == 0) {
			Serial.println("Turning off an entire star");
			for (int x = 0; x < 7; x++) {
			cyclotronLEDS[cyclotronElement - x] = CRGB::Black;
			}
		}
		// Increment the LED array
		cyclotronElement++;
		// If this is the last element in the array, reset it
		if((cyclotronElement) == cyclotron_NUM_LEDS) cyclotronElement = 0;
		// Update the timer
		cyclotronPreviousMillis = currentMillis;
	}

	// Powercell lights
	// If it's been powercellTiming ms since the last powercell action, do another action
	if(currentMillis - powercellPreviousMillis >= powercellTiming)
	{
		// Turn on the next LED
		powercellLEDS[powercellElement] = CRGB::Blue;
		FastLED.show();
		// If we aren't on the first LED, turn off the previous LED
		if(powercellElement >= 0) powercellLEDS[powercellElement] = CRGB::Black;
		// Increment the LED array
		powercellElement++;
		// If this is the last element in the array, reset it
		if((powercellElement) == powercell_NUM_LEDS) powercellElement = 0;
		// Update the timer
		powercellPreviousMillis = currentMillis;
	}
}