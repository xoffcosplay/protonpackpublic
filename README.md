# README #

Public files related to an Arduino-based Ghostbusters Proton Pack.

## Libraries Needed ##

This code requires the FastLED library: http://fastled.io/

Assuming you are using the Arduino IDE, you can just use the Library Manger to download it: https://www.arduino.cc/en/Guide/Libraries

## Parts Used ##

The project makes extensive use of the WS2812b or "Neopixel" RGB LED. You can (and should) buy them from Adafruit directly: https://www.adafruit.com/category/168

They are also available generically as the WS2812b LED on ebay, but remember while they're cheaper, you're often getting factory seconds, overruns, and/or untested units. YMMV.

Products Used:

* The Cyclotron LEDs are 7-LED "Jewels" https://www.adafruit.com/products/2226
* The Powercell LEDs are a length of around 18 high density strip Neopixels (density of 144 per meter): https://www.adafruit.com/products/2848

*NOTE:* These are not "paid" links. I don't make anything from you using them, they are just for informational purposes.